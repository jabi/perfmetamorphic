package randomfms;

import java.io.File;

import es.us.isa.FAMA.models.FAMAfeatureModel.FAMAFeatureModel;
import es.us.isa.generator.FM.AbstractFMGenerator;
import es.us.isa.generator.FM.FMGenerator;
import es.us.isa.generator.FM.GeneratorCharacteristics;
import es.us.isa.utils.FMWriter;

/**
 * ValidFMsWithBeTTy, adapted from BeTTy examples
 * 
 * @author jabier.martinez
 */
public class ValidFMsWithBeTTy {
	
	public static FAMAFeatureModel generateRandomFM(int numberOfFeatures, float ctcPercentage, int seed) {
		GeneratorCharacteristics characteristics = new GeneratorCharacteristics();
		characteristics.setNumberOfFeatures(numberOfFeatures); // Number of features
		
		characteristics.setPercentageCTC(ctcPercentage); // Percentage of cross-tree constraints.
		characteristics.setSeed(seed);

		// STEP 2: Generate the model with the specific characteristics (FaMa FM
		// metamodel is used)
		AbstractFMGenerator generator = new FMGenerator();
		FAMAFeatureModel fm = (FAMAFeatureModel) generator.generateFM(characteristics);

		return fm;
		
	}

	public static void generateValidFMsWithBeTTy(File outputFolder, int seed, int numberOfFMs, int numberOfFeatures,
			int numberOfConstraints) {
		outputFolder.mkdirs();
		for (int i = 1; i <= numberOfFMs; i++) {
			// STEP 1: Specify the user's preferences for the generation (so-called
			// characteristics)
			GeneratorCharacteristics characteristics = new GeneratorCharacteristics();
			characteristics.setNumberOfFeatures(numberOfFeatures); // Number of features
			float percentage = (float) Math.ceil((double) numberOfConstraints * 100 / (double) numberOfFeatures);
			characteristics.setPercentageCTC(percentage); // Percentage of cross-tree constraints.
			characteristics.setSeed(seed);

			// STEP 2: Generate the model with the specific characteristics (FaMa FM
			// metamodel is used)
			AbstractFMGenerator generator = new FMGenerator();
			FAMAFeatureModel fm = (FAMAFeatureModel) generator.generateFM(characteristics);
			System.out.println(fm.getFeaturesNumber());
			System.out.println(fm.toString());
			// STEP 3: Save the model
			FMWriter writer = new FMWriter();
			try {
				writer.saveFM(fm, outputFolder.getAbsolutePath() + "/fm" + getNumberWithZeros(i, numberOfFMs) + ".splx");
			} catch (Exception e) {
				e.printStackTrace();
			} // Other valid formats: .splx, .fm, .dot

			// add one to the seed
			seed += 1;
		}
	}

	private static String getNumberWithZeros(int i, int numberOfFMs) {
		int total = String.valueOf(numberOfFMs).length();
		String number = String.valueOf(i);
		int current = number.length();
		for(int x = current; x < total; x++) {
			number= "0" + number;
		}
		return number;
	}

}
