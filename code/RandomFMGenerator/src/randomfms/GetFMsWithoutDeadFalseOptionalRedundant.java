package randomfms;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;

import de.ovgu.featureide.fm.core.FeatureModelAnalyzer;
import de.ovgu.featureide.fm.core.base.IConstraint;
import de.ovgu.featureide.fm.core.base.IFeature;
import de.ovgu.featureide.fm.core.base.IFeatureModel;
import de.ovgu.featureide.fm.core.base.impl.DefaultFeatureModelFactory;
import de.ovgu.featureide.fm.core.base.impl.FeatureModel;
import de.ovgu.featureide.fm.core.io.sxfm.SXFMFormat;

/**
 * GetFMsWithoutDeadFalseOptionalRedundant
 * 
 * @author jabier.martinez
 */
public class GetFMsWithoutDeadFalseOptionalRedundant {

	public static void getFMWithoutDeadFalseOptionalRedundant(File inputFolder, File outputFile) {
		outputFile.getParentFile().mkdirs();
		for (File fmf : inputFolder.listFiles()) {
			if (fmf.getName().endsWith("xml")) {
				IFeatureModel fm = loadSXFM(fmf);
				FeatureModelAnalyzer analyzer = new FeatureModelAnalyzer(fm);
				boolean isValid = analyzer.isValid(null);
				List<IFeature> dead = analyzer.getDeadFeatures(null);
				List<IFeature> falseOptional = analyzer.getFalseOptionalFeatures(null);
				List<IConstraint> redundant = analyzer.getRedundantConstraints(null);
				if (isValid && dead.isEmpty() && falseOptional.isEmpty() && redundant.isEmpty()) {
					System.out.println("FOUND " + fmf.getAbsolutePath());
					copyFile(fmf, outputFile);
					return;
				} else {
					System.out.println(fmf.getAbsolutePath() + " NOT valid");
				}
			}
		}
		System.err.println("NO CORRECT FM FOUND IN THE FOLDER");
	}

	
	public static File getFMWithoutDeadFalseOptionalRedundant(File inputFolder) {
		//outputFile.getParentFile().mkdirs();
		for (File fmf : inputFolder.listFiles()) {
			if (fmf.getName().endsWith("xml")) {
				IFeatureModel fm = loadSXFM(fmf);
				FeatureModelAnalyzer analyzer = new FeatureModelAnalyzer(fm);
				boolean isValid = analyzer.isValid(null);
				List<IFeature> dead = analyzer.getDeadFeatures(null);
				List<IFeature> falseOptional = analyzer.getFalseOptionalFeatures(null);
				List<IConstraint> redundant = analyzer.getRedundantConstraints(null);
				if (isValid && dead.isEmpty() && falseOptional.isEmpty() && redundant.isEmpty()) {
					System.out.println("FOUND " + fmf.getAbsolutePath());
					//copyFile(fmf, outputFile);
					return fmf;
				} else {
					System.out.println(fmf.getAbsolutePath() + " NOT valid");
				}
			}
		}
		return null;
	}
	/**
	 * Load fm in fama
	 * 
	 * @param file
	 */
	public static IFeatureModel loadSXFM(File file) {
		SXFMFormat format = new SXFMFormat();
		IFeatureModel featureModel = new FeatureModel(DefaultFeatureModelFactory.ID);
		format.read(featureModel, PrepareToReadInFeatureIDE.getStringOfFile(file));
		return featureModel;
	}

	public static void copyFile(File sourceFile, File destinationFile) {
		destinationFile.mkdirs();
		try {
			Files.copy(sourceFile.toPath(), destinationFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
