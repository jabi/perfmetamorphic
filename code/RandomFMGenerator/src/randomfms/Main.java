package randomfms;

import java.io.File;

/**
 * Main
 * 
 * @author jabier.martinez
 */
public class Main {

	public static void main(String[] args) {

		int seed = 1;
		
		int[] numberOfFeatures = new int[] { 10, 20, 30 };
		int[] numberOfConstraints = new int[] { 1, 3, 5 };

		int numberOfFMs = 50;

		File temp = new File("temp");
		File resultFolder = new File("result");

		for (int f : numberOfFeatures) {
			for (int c : numberOfConstraints) {
				// Generate random fms
				File output = new File(temp, "fms_f" + f + "_c" + c);
				ValidFMsWithBeTTy.generateValidFMsWithBeTTy(output, seed, numberOfFMs, f, c);

				// Fix the format
				File fixed = new File(output.getParentFile(), output.getName() + "_fixed");
				PrepareToReadInFeatureIDE.prepareToReadInFeatureIDE(output, fixed);

				// Get the first correct one
				File result = new File(resultFolder, "fm_f" + f + "_c" + c + ".xml");
				GetFMsWithoutDeadFalseOptionalRedundant.getFMWithoutDeadFalseOptionalRedundant(fixed, result);
				
				// seed is increased here because otherwise it can be the same fm with extra
				// constraints
				seed = seed + numberOfFMs;
			}
		}
	}

}
