package randomfms;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * PrepareToReadInFeatureIDE
 * 
 * @author jabier.martinez
 */
public class PrepareToReadInFeatureIDE {

	public static void prepareToReadInFeatureIDE(File inputFolder, File outputFolder) {
		outputFolder.mkdirs();
		for (File fm : inputFolder.listFiles()) {
			if (fm.getName().endsWith("splx")) {
				String s = getStringOfFile(fm);
				// XXX to be removed when this is fixed
				// https://github.com/isa-group/BeTTy/issues/4
				s = s.replaceAll("r:", ":r");
				// XXX to be removed when this is fixed
				// https://github.com/FeatureIDE/FeatureIDE/issues/1009
				for (int i = 2; i < 100; i++) {
					s = s.replaceAll("\\[1," + i + "]", "[1,*]");
				}
				File f = new File(outputFolder, fm.getName() + ".xml");
				writeStringToFile(f, s);
			}
		}
	}

	public static void writeStringToFile(File file, String text) {
		try {
			BufferedWriter output;
			output = new BufferedWriter(new FileWriter(file, false));
			output.append(text);
			output.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get lines of a file
	 * 
	 * @param file
	 * @return list of strings
	 */
	public static List<String> getLinesOfFile(File file) {
		List<String> lines = new ArrayList<String>();
		try {
			FileInputStream fstream = new FileInputStream(file);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			while ((strLine = br.readLine()) != null) {
				lines.add(strLine);
			}
			br.close();
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lines;
	}

	/**
	 * Get string
	 * 
	 * @param file
	 * @return
	 */
	public static String getStringOfFile(File file) {
		StringBuilder string = new StringBuilder();
		for (String line : getLinesOfFile(file)) {
			string.append(line + "\n");
		}
		if (string.length() > 0) // If the file is empty the -1 causes an exception
			string.setLength(string.length() - 1);
		return string.toString();
	}
}
