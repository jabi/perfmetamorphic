package de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration;

import java.util.List;

import de.ovgu.featureide.fm.core.analysis.cnf.CNF;
import de.ovgu.featureide.fm.core.analysis.cnf.LiteralSet;
import de.ovgu.featureide.fm.core.analysis.cnf.formula.FeatureModelFormula;
import de.ovgu.featureide.fm.core.base.IFeatureModel;
import de.ovgu.featureide.fm.core.job.monitor.NullMonitor;
import de.ovgu.featureide.fm.core_perftest.AbstractFMOperatorPerfMTest;

public class Sampling_OneWise_PerfMTest extends AbstractFMOperatorPerfMTest {

	OneWiseConfigurationGenerator operator;

	@Override
	public void prepareOperator(IFeatureModel fm) {
		FeatureModelFormula fmFormula = new FeatureModelFormula(fm);
		CNF cnf = fmFormula.getCNF();
		operator = new OneWiseConfigurationGenerator(cnf);
	}

	@Override
	public void executeOperator() {
		try {
			operator.analyze(new NullMonitor<List<LiteralSet>>());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
