package de.ovgu.featureide.fm.core;

import de.ovgu.featureide.fm.core.base.IFeatureModel;
import de.ovgu.featureide.fm.core.job.monitor.NullMonitor;
import de.ovgu.featureide.fm.core_perftest.AbstractFMOperatorPerfMTest;

public class ValidFMPerfMTest extends AbstractFMOperatorPerfMTest {

	FeatureModelAnalyzer operator;

	@Override
	public void prepareOperator(IFeatureModel fm) {
		 operator = new FeatureModelAnalyzer(fm);
	}

	@Override
	public void executeOperator() {
		operator.isValid(new NullMonitor<Boolean>());
	}

}
