package de.ovgu.featureide.fm.core;

import de.ovgu.featureide.fm.core.analysis.cnf.LiteralSet;
import de.ovgu.featureide.fm.core.base.IFeatureModel;
import de.ovgu.featureide.fm.core.job.monitor.NullMonitor;
import de.ovgu.featureide.fm.core_perftest.AbstractFMOperatorRealFMPerfMTest;

public class CoreFeaturesRealFMPerfMTest extends AbstractFMOperatorRealFMPerfMTest {

	FeatureModelAnalyzer operator;

	@Override
	public void prepareOperator(IFeatureModel fm) {
		 operator = new FeatureModelAnalyzer(fm);
	}

	@Override
	public void executeOperator() {
		operator.getCoreFeatures(new NullMonitor<LiteralSet>());
	}

}
