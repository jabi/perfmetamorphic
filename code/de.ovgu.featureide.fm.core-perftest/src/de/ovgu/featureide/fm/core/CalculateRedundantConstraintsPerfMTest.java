package de.ovgu.featureide.fm.core;

import java.util.List;

import de.ovgu.featureide.fm.core.analysis.cnf.LiteralSet;
import de.ovgu.featureide.fm.core.base.IFeatureModel;
import de.ovgu.featureide.fm.core.job.monitor.NullMonitor;
import de.ovgu.featureide.fm.core_perftest.AbstractFMOperatorPerfMTest;

public class CalculateRedundantConstraintsPerfMTest extends AbstractFMOperatorPerfMTest {

	FeatureModelAnalyzer operator;

	@Override
	public void prepareOperator(IFeatureModel fm) {
		 operator = new FeatureModelAnalyzer(fm);
	}

	@Override
	public void executeOperator() {
		operator.getRedundantConstraints(new NullMonitor<List<LiteralSet>>());
	}

}
