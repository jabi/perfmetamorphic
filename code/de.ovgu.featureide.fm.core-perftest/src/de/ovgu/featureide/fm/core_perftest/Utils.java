package de.ovgu.featureide.fm.core_perftest;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import de.ovgu.featureide.fm.core.base.IFeatureModel;
import de.ovgu.featureide.fm.core.base.impl.DefaultFeatureModelFactory;
import de.ovgu.featureide.fm.core.base.impl.FMFormatManager;
import de.ovgu.featureide.fm.core.base.impl.FeatureModel;
import de.ovgu.featureide.fm.core.io.IPersistentFormat;
import de.ovgu.featureide.fm.core.io.manager.FeatureModelManager;
import de.ovgu.featureide.fm.core.io.manager.FileHandler;
import de.ovgu.featureide.fm.core.io.sxfm.SXFMFormat;
import de.ovgu.featureide.fm.core.io.xml.XmlFeatureModelFormat;

public class Utils {

	/**
	 * Get a fm with a root and x optional features as children
	 * 
	 * @param xFeatures
	 * @return
	 */
	public static String getFlatFM(int xFeatures) {
		String features = "";
		for (int i = 1; i <= xFeatures; i++) {
			features += "<feature mandatory=\"false\" name=\"F" + i + "\"/>";
		}
		String fm = "<featureModel><struct> <and mandatory=\"true\" name=\"Root\">" + features;
		fm += "</and> </struct>";
		fm += "</featureModel>";
		return fm;
	}

	/**
	 * Load fm as string
	 * 
	 * @param file
	 */
	public static IFeatureModel load(String content) {
		IFeatureModel featureModel = new FeatureModel(DefaultFeatureModelFactory.ID);
		FMFormatManager.getInstance().addExtension(new XmlFeatureModelFormat());
		IPersistentFormat<IFeatureModel> format = FMFormatManager.getInstance().getFormatByContent(content, "a.xml");
		FileHandler.loadFromString(content, featureModel, format);
		return featureModel;
	}

	/**
	 * Load fm
	 * 
	 * @param file
	 */
	public static IFeatureModel load(File file) {
		IFeatureModel fm = FeatureModelManager.load(file.toPath());
		return fm;
	}

	/**
	 * Load fm in fama
	 * 
	 * @param file
	 */
	public static IFeatureModel loadSXFM(File file) {
		SXFMFormat format = new SXFMFormat();
		IFeatureModel featureModel = new FeatureModel(DefaultFeatureModelFactory.ID);
		format.read(featureModel, getStringOfFile(file));
		return featureModel;
	}
	
	public static void main(String[] args) {
		// File f = new File("fms_betty_100f_10percCTC_valid/a.xml");
		// File f = new File("fms_betty_100f_10percCTC_valid/FeatureModel1.xml");
		File f = new File("fms_betty_100f_10percCTC_valid/a.xml");
		IFeatureModel fm = loadSXFM(f);
		System.out.println(fm);
		System.out.println(fm.getConstraintCount());
		System.out.println(fm.getConstraints());
	}

	public static void saveFM(File newFile, IFeatureModel fm) {
		XmlFeatureModelFormat format = new XmlFeatureModelFormat();
		String fmString = format.write(fm);
		writeStringToFile(newFile, fmString);
	}

	public static void writeStringToFile(File file, String text) {
		try {
			BufferedWriter output;
			output = new BufferedWriter(new FileWriter(file, false));
			output.append(text);
			output.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get lines of a file
	 * 
	 * @param file
	 * @return list of strings
	 */
	public static List<String> getLinesOfFile(File file) {
		List<String> lines = new ArrayList<String>();
		try {
			FileInputStream fstream = new FileInputStream(file);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			while ((strLine = br.readLine()) != null) {
				lines.add(strLine);
			}
			br.close();
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lines;
	}

	/**
	 * Get string
	 * 
	 * @param file
	 * @return
	 */
	public static String getStringOfFile(File file) {
		StringBuilder string = new StringBuilder();
		for (String line : getLinesOfFile(file)) {
			string.append(line + "\n");
		}
		if (string.length() > 0) // If the file is empty the -1 causes an exception
			string.setLength(string.length() - 1);
		return string.toString();
	}

}
