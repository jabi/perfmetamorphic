package de.ovgu.featureide.fm.core_perftest;

import java.io.File;
import java.text.DecimalFormat;

import de.ovgu.featureide.fm.core.AtomicSetsFMPerfMTest;
import de.ovgu.featureide.fm.core.CalculateRedundantConstraintsPerfMTest;
import de.ovgu.featureide.fm.core.CoreFeaturesPerfMTest;
import de.ovgu.featureide.fm.core.CoreFeaturesRealFMPerfMTest;
import de.ovgu.featureide.fm.core.CountSolutionsPerfMTest;
import de.ovgu.featureide.fm.core.DeadFeaturesPerfMTest;
import de.ovgu.featureide.fm.core.FalseOptionalFeaturesPerfMTest;
import de.ovgu.featureide.fm.core.ValidFMPerfMTest;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.Sampling_AllConfigurations_PerfMTest;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.Sampling_Chvatal_PerfMTest;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.Sampling_ICPL_PerfMTest;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.Sampling_OneWise_PerfMTest;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.Sampling_PairWiseIncLing_PerfMTest;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.Sampling_PairWiseYasa_PerfMTest;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.Sampling_Random_PerfMTest;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.Sampling_ThreeWiseYasa_PerfMTest;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.T_Chvatal_PerfMTest;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.T_ICPL_PerfMTest;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.T_Yasa_PerfMTest;

public class ExecuteExperimentsPaper {

	static int NUMS_OF_EXPERIMENT_RUNS = 30;
	private static DecimalFormat df3 = new DecimalFormat("#.###");
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launchExperimentsConfgsGeneration();
		
	}
	
	private static void launchExperimentsReasoners() {
		double[][] EXPERIMENTAL_DATA_PVALS =  new double[NUMS_OF_EXPERIMENT_RUNS][AbstractFMOperatorPerfMTest.NUMBER_OF_FMs];
		double[][] EXPERIMENTAL_DATA_TTESTVALS =  new double[NUMS_OF_EXPERIMENT_RUNS][AbstractFMOperatorPerfMTest.NUMBER_OF_FMs];
		long[][] EXPERIMENTAL_DATA_AVERAGE_FM = new long[NUMS_OF_EXPERIMENT_RUNS][AbstractFMOperatorPerfMTest.NUMBER_OF_FMs];
		long[][] EXPERIMENTAL_DATA_AVERAGE_FMFU = new long[NUMS_OF_EXPERIMENT_RUNS][AbstractFMOperatorPerfMTest.NUMBER_OF_FMs];
		boolean[][] EXPERIMENTAL_DATA_VERDICTS = new boolean[NUMS_OF_EXPERIMENT_RUNS][AbstractFMOperatorPerfMTest.NUMBER_OF_FMs];
		boolean[][] EXPERIMENTAL_DATA_ISNORMAL = new boolean[NUMS_OF_EXPERIMENT_RUNS][AbstractFMOperatorPerfMTest.NUMBER_OF_FMs];
		
		StringBuilder csv = new StringBuilder();
		csv.append("Operation");
		csv.append(";");
		csv.append("runNumber");
		csv.append(";");
		csv.append("tcNumber");
		csv.append(";");
		csv.append("pval");
		csv.append(";");
		csv.append("ttestval");
		csv.append(";");
		csv.append("avr. time fm");
		csv.append(";");
		csv.append("avg. time fmu");
		csv.append(";");
		csv.append("verdict");
		csv.append(";");
		csv.append("normality");
		csv.append("\n");
		
		for(int i = 0;i< NUMS_OF_EXPERIMENT_RUNS; i++) {
			AtomicSetsFMPerfMTest conf = new AtomicSetsFMPerfMTest();
			
			conf.test();
			
			
			for(int j = 0; j<AbstractFMOperatorPerfMTest.NUMBER_OF_FMs; j++) {
				EXPERIMENTAL_DATA_PVALS[i][j] = conf.EXPERIMENT_pvalVals[j];
				EXPERIMENTAL_DATA_TTESTVALS[i][j] = conf.EXPERIMENT_ttestVals[j];
				EXPERIMENTAL_DATA_AVERAGE_FM[i][j] = conf.EXPERIMENT_averageTimeVals_fmTimes[j];
				EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j] = conf.EXPERIMENT_averageTimeVals_fmfuTimes[j];
				EXPERIMENTAL_DATA_VERDICTS[i][j] = conf.EXPERIMENT_verdictsVals[j];
				EXPERIMENTAL_DATA_ISNORMAL[i][j] = conf.EXPERIMENT_isNormalVals[j];
				
				csv.append("AtomicSetsFMPerfMTest");
				csv.append(";");
				csv.append(i);
				csv.append(";");
				csv.append(j);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_PVALS[i][j])));//EXPERIMENTAL_DATA_PVALS[i][j]);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_TTESTVALS[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FM[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j])));
				csv.append(";");
				csv.append(String.valueOf((EXPERIMENTAL_DATA_VERDICTS[i][j]==true)?"PASS":"FAIL"));
				csv.append(";");
				csv.append(String.valueOf(EXPERIMENTAL_DATA_ISNORMAL[i][j]));
				csv.append("\n");
			}			
		}
		double percFlaky_AtomicSetsFMPerfMTest=  getPercentageOfFlakyTests(EXPERIMENTAL_DATA_VERDICTS);
		double percStatisticalSign_AtomicSetsFMPerfMTest = getPercentageOfStatisticalSignificance(EXPERIMENTAL_DATA_PVALS);
		
	
		
		for(int i = 0;i< NUMS_OF_EXPERIMENT_RUNS; i++) {
			CalculateRedundantConstraintsPerfMTest conf = new CalculateRedundantConstraintsPerfMTest();
			
			conf.test();
			
			
			for(int j = 0; j<AbstractFMOperatorPerfMTest.NUMBER_OF_FMs; j++) {
				EXPERIMENTAL_DATA_PVALS[i][j] = conf.EXPERIMENT_pvalVals[j];
				EXPERIMENTAL_DATA_TTESTVALS[i][j] = conf.EXPERIMENT_ttestVals[j];
				EXPERIMENTAL_DATA_AVERAGE_FM[i][j] = conf.EXPERIMENT_averageTimeVals_fmTimes[j];
				EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j] = conf.EXPERIMENT_averageTimeVals_fmfuTimes[j];
				EXPERIMENTAL_DATA_VERDICTS[i][j] = conf.EXPERIMENT_verdictsVals[j];
				EXPERIMENTAL_DATA_ISNORMAL[i][j] = conf.EXPERIMENT_isNormalVals[j];
				
				csv.append("CalculateRedundantConstraintsPerfMTest");
				csv.append(";");
				csv.append(i);
				csv.append(";");
				csv.append(j);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_PVALS[i][j])));//EXPERIMENTAL_DATA_PVALS[i][j]);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_TTESTVALS[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FM[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j])));
				csv.append(";");
				csv.append(String.valueOf((EXPERIMENTAL_DATA_VERDICTS[i][j]==true)?"PASS":"FAIL"));
				csv.append(";");
				csv.append(String.valueOf(EXPERIMENTAL_DATA_ISNORMAL[i][j]));
				csv.append("\n");
			}			
		}
		double percFlaky_CalculateRedundantConstraintsPerfMTest=  getPercentageOfFlakyTests(EXPERIMENTAL_DATA_VERDICTS);
		double percStatisticalSign_CalculateRedundantConstraintsPerfMTest = getPercentageOfStatisticalSignificance(EXPERIMENTAL_DATA_PVALS);
		
		
		/*
		//TODO: casca!
		for(int i = 0;i< NUMS_OF_EXPERIMENT_RUNS; i++) {
			CoreFeaturesBettyPerfMTest conf = new CoreFeaturesBettyPerfMTest();
			
			conf.test();
			
			
			for(int j = 0; j<AbstractFMOperatorPerfMTest.NUMBER_OF_FMs; j++) {
				EXPERIMENTAL_DATA_PVALS[i][j] = conf.EXPERIMENT_pvalVals[j];
				EXPERIMENTAL_DATA_TTESTVALS[i][j] = conf.EXPERIMENT_ttestVals[j];
				EXPERIMENTAL_DATA_AVERAGE_FM[i][j] = conf.EXPERIMENT_averageTimeVals_fmTimes[j];
				EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j] = conf.EXPERIMENT_averageTimeVals_fmfuTimes[j];
				EXPERIMENTAL_DATA_VERDICTS[i][j] = conf.EXPERIMENT_verdictsVals[j];
				EXPERIMENTAL_DATA_ISNORMAL[i][j] = conf.EXPERIMENT_isNormalVals[j];
				
				csv.append("CoreFeaturesBettyPerfMTest");
				csv.append(";");
				csv.append(i);
				csv.append(";");
				csv.append(j);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_PVALS[i][j])));//EXPERIMENTAL_DATA_PVALS[i][j]);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_TTESTVALS[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FM[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j])));
				csv.append(";");
				csv.append(String.valueOf((EXPERIMENTAL_DATA_VERDICTS[i][j]==true)?"PASS":"FAIL"));
				csv.append(";");
				csv.append(String.valueOf(EXPERIMENTAL_DATA_ISNORMAL[i][j]));
				csv.append("\n");
			}			
		}
		double percFlaky_CoreFeaturesBettyPerfMTest=  getPercentageOfFlakyTests(EXPERIMENTAL_DATA_VERDICTS);
		double percStatisticalSign_CoreFeaturesBettyPerfMTest = getPercentageOfStatisticalSignificance(EXPERIMENTAL_DATA_PVALS);
		*/

		
		
		
		for(int i = 0;i< NUMS_OF_EXPERIMENT_RUNS; i++) {
			CoreFeaturesPerfMTest conf = new CoreFeaturesPerfMTest();
			
			conf.test();
			
			
			for(int j = 0; j<AbstractFMOperatorPerfMTest.NUMBER_OF_FMs; j++) {
				EXPERIMENTAL_DATA_PVALS[i][j] = conf.EXPERIMENT_pvalVals[j];
				EXPERIMENTAL_DATA_TTESTVALS[i][j] = conf.EXPERIMENT_ttestVals[j];
				EXPERIMENTAL_DATA_AVERAGE_FM[i][j] = conf.EXPERIMENT_averageTimeVals_fmTimes[j];
				EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j] = conf.EXPERIMENT_averageTimeVals_fmfuTimes[j];
				EXPERIMENTAL_DATA_VERDICTS[i][j] = conf.EXPERIMENT_verdictsVals[j];
				EXPERIMENTAL_DATA_ISNORMAL[i][j] = conf.EXPERIMENT_isNormalVals[j];
				
				csv.append("CoreFeaturesPerfMTest");
				csv.append(";");
				csv.append(i);
				csv.append(";");
				csv.append(j);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_PVALS[i][j])));//EXPERIMENTAL_DATA_PVALS[i][j]);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_TTESTVALS[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FM[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j])));
				csv.append(";");
				csv.append(String.valueOf((EXPERIMENTAL_DATA_VERDICTS[i][j]==true)?"PASS":"FAIL"));
				csv.append(";");
				csv.append(String.valueOf(EXPERIMENTAL_DATA_ISNORMAL[i][j]));
				csv.append("\n");
			}			
		}
		double percFlaky_CoreFeaturesPerfMTest=  getPercentageOfFlakyTests(EXPERIMENTAL_DATA_VERDICTS);
		double percStatisticalSign_CoreFeaturesPerfMTest = getPercentageOfStatisticalSignificance(EXPERIMENTAL_DATA_PVALS);
		
		
		
		/*TODO: check --> casca
		for(int i = 0;i< NUMS_OF_EXPERIMENT_RUNS; i++) {
			CoreFeaturesRealFMPerfMTest conf = new CoreFeaturesRealFMPerfMTest();
			
			conf.test();
			
			
			for(int j = 0; j<AbstractFMOperatorPerfMTest.NUMBER_OF_FMs; j++) {
				EXPERIMENTAL_DATA_PVALS[i][j] = conf.EXPERIMENT_pvalVals[j];
				EXPERIMENTAL_DATA_TTESTVALS[i][j] = conf.EXPERIMENT_ttestVals[j];
				EXPERIMENTAL_DATA_AVERAGE_FM[i][j] = conf.EXPERIMENT_averageTimeVals_fmTimes[j];
				EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j] = conf.EXPERIMENT_averageTimeVals_fmfuTimes[j];
				EXPERIMENTAL_DATA_VERDICTS[i][j] = conf.EXPERIMENT_verdictsVals[j];
				EXPERIMENTAL_DATA_ISNORMAL[i][j] = conf.EXPERIMENT_isNormalVals[j];
			}			
		}
		double percFlaky_CoreFeaturesRealFMPerfMTest =  getPercentageOfFlakyTests(EXPERIMENTAL_DATA_VERDICTS);
		double percStatisticalSign_CoreFeaturesRealFMPerfMTest = getPercentageOfStatisticalSignificance(EXPERIMENTAL_DATA_PVALS);
		*/	
		
		
		
		for(int i = 0;i< NUMS_OF_EXPERIMENT_RUNS; i++) {
			CountSolutionsPerfMTest conf = new CountSolutionsPerfMTest();
			
			conf.test();
			
			
			for(int j = 0; j<AbstractFMOperatorPerfMTest.NUMBER_OF_FMs; j++) {
				EXPERIMENTAL_DATA_PVALS[i][j] = conf.EXPERIMENT_pvalVals[j];
				EXPERIMENTAL_DATA_TTESTVALS[i][j] = conf.EXPERIMENT_ttestVals[j];
				EXPERIMENTAL_DATA_AVERAGE_FM[i][j] = conf.EXPERIMENT_averageTimeVals_fmTimes[j];
				EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j] = conf.EXPERIMENT_averageTimeVals_fmfuTimes[j];
				EXPERIMENTAL_DATA_VERDICTS[i][j] = conf.EXPERIMENT_verdictsVals[j];
				EXPERIMENTAL_DATA_ISNORMAL[i][j] = conf.EXPERIMENT_isNormalVals[j];
				
				csv.append("CountSolutionsPerfMTest");
				csv.append(";");
				csv.append(i);
				csv.append(";");
				csv.append(j);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_PVALS[i][j])));//EXPERIMENTAL_DATA_PVALS[i][j]);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_TTESTVALS[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FM[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j])));
				csv.append(";");
				csv.append(String.valueOf((EXPERIMENTAL_DATA_VERDICTS[i][j]==true)?"PASS":"FAIL"));
				csv.append(";");
				csv.append(String.valueOf(EXPERIMENTAL_DATA_ISNORMAL[i][j]));
				csv.append("\n");
			}			
		}
		double percFlaky_CountSolutionsPerfMTest =  getPercentageOfFlakyTests(EXPERIMENTAL_DATA_VERDICTS);
		double percStatisticalSign_CountSolutionsPerfMTest = getPercentageOfStatisticalSignificance(EXPERIMENTAL_DATA_PVALS);
		
		
		
		
		for(int i = 0;i< NUMS_OF_EXPERIMENT_RUNS; i++) {
			DeadFeaturesPerfMTest conf = new DeadFeaturesPerfMTest();
			
			conf.test();
			
			
			for(int j = 0; j<AbstractFMOperatorPerfMTest.NUMBER_OF_FMs; j++) {
				EXPERIMENTAL_DATA_PVALS[i][j] = conf.EXPERIMENT_pvalVals[j];
				EXPERIMENTAL_DATA_TTESTVALS[i][j] = conf.EXPERIMENT_ttestVals[j];
				EXPERIMENTAL_DATA_AVERAGE_FM[i][j] = conf.EXPERIMENT_averageTimeVals_fmTimes[j];
				EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j] = conf.EXPERIMENT_averageTimeVals_fmfuTimes[j];
				EXPERIMENTAL_DATA_VERDICTS[i][j] = conf.EXPERIMENT_verdictsVals[j];
				EXPERIMENTAL_DATA_ISNORMAL[i][j] = conf.EXPERIMENT_isNormalVals[j];
				
				csv.append("DeadFeaturesPerfMTest");
				csv.append(";");
				csv.append(i);
				csv.append(";");
				csv.append(j);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_PVALS[i][j])));//EXPERIMENTAL_DATA_PVALS[i][j]);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_TTESTVALS[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FM[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j])));
				csv.append(";");
				csv.append(String.valueOf((EXPERIMENTAL_DATA_VERDICTS[i][j]==true)?"PASS":"FAIL"));
				csv.append(";");
				csv.append(String.valueOf(EXPERIMENTAL_DATA_ISNORMAL[i][j]));
				csv.append("\n");
			}			
		}
		double percFlaky_DeadFeaturesPerfMTest =  getPercentageOfFlakyTests(EXPERIMENTAL_DATA_VERDICTS);
		double percStatisticalSign_DeadFeaturesPerfMTest = getPercentageOfStatisticalSignificance(EXPERIMENTAL_DATA_PVALS);
		
		
		
		
		for(int i = 0;i< NUMS_OF_EXPERIMENT_RUNS; i++) {
			ValidFMPerfMTest conf = new ValidFMPerfMTest();
			
			conf.test();
			
			
			for(int j = 0; j<AbstractFMOperatorPerfMTest.NUMBER_OF_FMs; j++) {
				EXPERIMENTAL_DATA_PVALS[i][j] = conf.EXPERIMENT_pvalVals[j];
				EXPERIMENTAL_DATA_TTESTVALS[i][j] = conf.EXPERIMENT_ttestVals[j];
				EXPERIMENTAL_DATA_AVERAGE_FM[i][j] = conf.EXPERIMENT_averageTimeVals_fmTimes[j];
				EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j] = conf.EXPERIMENT_averageTimeVals_fmfuTimes[j];
				EXPERIMENTAL_DATA_VERDICTS[i][j] = conf.EXPERIMENT_verdictsVals[j];
				EXPERIMENTAL_DATA_ISNORMAL[i][j] = conf.EXPERIMENT_isNormalVals[j];
				
				csv.append("ValidFMPerfMTest");
				csv.append(";");
				csv.append(i);
				csv.append(";");
				csv.append(j);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_PVALS[i][j])));//EXPERIMENTAL_DATA_PVALS[i][j]);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_TTESTVALS[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FM[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j])));
				csv.append(";");
				csv.append(String.valueOf((EXPERIMENTAL_DATA_VERDICTS[i][j]==true)?"PASS":"FAIL"));
				csv.append(";");
				csv.append(String.valueOf(EXPERIMENTAL_DATA_ISNORMAL[i][j]));
				csv.append("\n");
			}			
		}
		double percFlaky_ValidFMPerfMTest =  getPercentageOfFlakyTests(EXPERIMENTAL_DATA_VERDICTS);
		double percStatisticalSign_ValidFMPerfMTest = getPercentageOfStatisticalSignificance(EXPERIMENTAL_DATA_PVALS);
		
		for(int i = 0;i< NUMS_OF_EXPERIMENT_RUNS; i++) {
			FalseOptionalFeaturesPerfMTest conf = new FalseOptionalFeaturesPerfMTest();
			
			conf.test();
			
			
			for(int j = 0; j<AbstractFMOperatorPerfMTest.NUMBER_OF_FMs; j++) {
				EXPERIMENTAL_DATA_PVALS[i][j] = conf.EXPERIMENT_pvalVals[j];
				EXPERIMENTAL_DATA_TTESTVALS[i][j] = conf.EXPERIMENT_ttestVals[j];
				EXPERIMENTAL_DATA_AVERAGE_FM[i][j] = conf.EXPERIMENT_averageTimeVals_fmTimes[j];
				EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j] = conf.EXPERIMENT_averageTimeVals_fmfuTimes[j];
				EXPERIMENTAL_DATA_VERDICTS[i][j] = conf.EXPERIMENT_verdictsVals[j];
				EXPERIMENTAL_DATA_ISNORMAL[i][j] = conf.EXPERIMENT_isNormalVals[j];
				
				csv.append("FalseOptionalFeaturesPerfMTest");
				csv.append(";");
				csv.append(i);
				csv.append(";");
				csv.append(j);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_PVALS[i][j])));//EXPERIMENTAL_DATA_PVALS[i][j]);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_TTESTVALS[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FM[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j])));
				csv.append(";");
				csv.append(String.valueOf((EXPERIMENTAL_DATA_VERDICTS[i][j]==true)?"PASS":"FAIL"));
				csv.append(";");
				csv.append(String.valueOf(EXPERIMENTAL_DATA_ISNORMAL[i][j]));
				csv.append("\n");
			}			
		}
		
		double percFlaky_FalseOptionalFeaturesPerfMTest =  getPercentageOfFlakyTests(EXPERIMENTAL_DATA_VERDICTS);
		double percStatisticalSign_FalseOptionalFeaturesPerfMTest = getPercentageOfStatisticalSignificance(EXPERIMENTAL_DATA_PVALS);

		
		
		System.out.println("percFlaky_AtomicSetsFMPerfMTest = " + percFlaky_AtomicSetsFMPerfMTest);
		System.out.println("percFlaky_CalculateRedundantConstraintsPerfMTest = " + percFlaky_CalculateRedundantConstraintsPerfMTest);
		//System.out.println("percFlaky_CoreFeaturesBettyPerfMTest = "+ percFlaky_CoreFeaturesBettyPerfMTest);
		System.out.println("percFlaky_CoreFeaturesPerfMTest = "+ percFlaky_CoreFeaturesPerfMTest);
		//System.out.println("percFlaky_CoreFeaturesRealFMPerfMTest = " +percFlaky_CoreFeaturesRealFMPerfMTest);
		System.out.println("percFlaky_CountSolutionsPerfMTest = " +percFlaky_CountSolutionsPerfMTest);
		System.out.println("percFlaky_DeadFeaturesPerfMTest = " +percFlaky_DeadFeaturesPerfMTest);
		System.out.println("percFlaky_ValidFMPerfMTest = " +percFlaky_ValidFMPerfMTest);
		System.out.println("percFlaky_FalseOptionalFeaturesPerfMTest = " + percFlaky_FalseOptionalFeaturesPerfMTest);
		
		
		System.out.println("percStatisticalSign_AtomicSetsFMPerfMTest = " +	percStatisticalSign_AtomicSetsFMPerfMTest);
		System.out.println("percStatisticalSign_CalculateRedundantConstraintsPerfMTest	 = " +	percStatisticalSign_CalculateRedundantConstraintsPerfMTest);
		//System.out.println("percStatisticalSign_CoreFeaturesBettyPerfMTest	 = " +	percStatisticalSign_CoreFeaturesBettyPerfMTest);
		System.out.println("percStatisticalSign_CoreFeaturesPerfMTest = " +	percStatisticalSign_CoreFeaturesPerfMTest);
		//System.out.println("percStatisticalSign_CoreFeaturesRealFMPerfMTest = " +percStatisticalSign_CoreFeaturesRealFMPerfMTest);
		System.out.println("percStatisticalSign_CountSolutionsPerfMTest = " +percStatisticalSign_CountSolutionsPerfMTest);
		System.out.println("percStatisticalSign_DeadFeaturesPerfMTest = " +	percStatisticalSign_DeadFeaturesPerfMTest);
		System.out.println("percStatisticalSign_ValidFMPerfMTest = " +percStatisticalSign_ValidFMPerfMTest);
		System.out.println("percStatisticalSign_FalseOptionalFeaturesPerfMTest = " + percStatisticalSign_FalseOptionalFeaturesPerfMTest);
		
		
		File output = new File("output/EXPERIMENTS_SPLC2020");// + this.getClass().getSimpleName());
		if (!output.exists()) {
			output.mkdirs();
		}
		
		
		File newFile = new File(output, "reasonersExperiments.csv");
		Utils.writeStringToFile(newFile, csv.toString());
		
	}
	
	
	private static void launchExperimentsConfgsGeneration() {
		
		double[][] EXPERIMENTAL_DATA_PVALS =  new double[NUMS_OF_EXPERIMENT_RUNS][AbstractFMOperatorPerfMTest.NUMBER_OF_FMs];
		double[][] EXPERIMENTAL_DATA_TTESTVALS =  new double[NUMS_OF_EXPERIMENT_RUNS][AbstractFMOperatorPerfMTest.NUMBER_OF_FMs];
		long[][] EXPERIMENTAL_DATA_AVERAGE_FM = new long[NUMS_OF_EXPERIMENT_RUNS][AbstractFMOperatorPerfMTest.NUMBER_OF_FMs];
		long[][] EXPERIMENTAL_DATA_AVERAGE_FMFU = new long[NUMS_OF_EXPERIMENT_RUNS][AbstractFMOperatorPerfMTest.NUMBER_OF_FMs];
		boolean[][] EXPERIMENTAL_DATA_VERDICTS = new boolean[NUMS_OF_EXPERIMENT_RUNS][AbstractFMOperatorPerfMTest.NUMBER_OF_FMs];
		boolean[][] EXPERIMENTAL_DATA_ISNORMAL = new boolean[NUMS_OF_EXPERIMENT_RUNS][AbstractFMOperatorPerfMTest.NUMBER_OF_FMs];
		
		StringBuilder csv = new StringBuilder();
		csv.append("Operation");
		csv.append(";");
		csv.append("runNumber");
		csv.append(";");
		csv.append("tcNumber");
		csv.append(";");
		csv.append("pval");
		csv.append(";");
		csv.append("ttestval");
		csv.append(";");
		csv.append("avr. time fm");
		csv.append(";");
		csv.append("avg. time fmu");
		csv.append(";");
		csv.append("verdict");
		csv.append(";");
		csv.append("normality");
		csv.append("\n");
		
		//Experiments from AllConfigurationsSamplingPerfMTest
		
		for(int i = 0;i< NUMS_OF_EXPERIMENT_RUNS; i++) {
			Sampling_AllConfigurations_PerfMTest conf = new Sampling_AllConfigurations_PerfMTest();
			
			conf.test();
			
			System.out.println("Run number in Sampling all configurations = " + i);
			for(int j = 0; j<AbstractFMOperatorPerfMTest.NUMBER_OF_FMs; j++) {
				EXPERIMENTAL_DATA_PVALS[i][j] = conf.EXPERIMENT_pvalVals[j];
				EXPERIMENTAL_DATA_TTESTVALS[i][j] = conf.EXPERIMENT_ttestVals[j];
				EXPERIMENTAL_DATA_AVERAGE_FM[i][j] = conf.EXPERIMENT_averageTimeVals_fmTimes[j];
				EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j] = conf.EXPERIMENT_averageTimeVals_fmfuTimes[j];
				EXPERIMENTAL_DATA_VERDICTS[i][j] = conf.EXPERIMENT_verdictsVals[j];
				EXPERIMENTAL_DATA_ISNORMAL[i][j] = conf.EXPERIMENT_isNormalVals[j];
				
				csv.append("Sampling_AllConfigurations_PerfMTest");
				csv.append(";");
				csv.append(i);
				csv.append(";");
				csv.append(j);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_PVALS[i][j])));//EXPERIMENTAL_DATA_PVALS[i][j]);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_TTESTVALS[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FM[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j])));
				csv.append(";");
				csv.append(String.valueOf((EXPERIMENTAL_DATA_VERDICTS[i][j]==true)?"PASS":"FAIL"));
				csv.append(";");
				csv.append(String.valueOf(EXPERIMENTAL_DATA_ISNORMAL[i][j]));
				csv.append("\n");
			}			
		}
		double percFlaky_AllConfigurationsSamplingPerfMTest=  getPercentageOfFlakyTests(EXPERIMENTAL_DATA_VERDICTS);
		double percStatisticalSign_AllConfigurationsSamplingPerfMTest = getPercentageOfStatisticalSignificance(EXPERIMENTAL_DATA_PVALS);
		
		//Experiments from ChvatalSamplingPerfMTest
		
		for(int i = 0;i< NUMS_OF_EXPERIMENT_RUNS; i++) {
			Sampling_Chvatal_PerfMTest conf = new Sampling_Chvatal_PerfMTest();
			
			conf.test();
			
			
			for(int j = 0; j<AbstractFMOperatorPerfMTest.NUMBER_OF_FMs; j++) {
				EXPERIMENTAL_DATA_PVALS[i][j] = conf.EXPERIMENT_pvalVals[j];
				EXPERIMENTAL_DATA_TTESTVALS[i][j] = conf.EXPERIMENT_ttestVals[j];
				EXPERIMENTAL_DATA_AVERAGE_FM[i][j] = conf.EXPERIMENT_averageTimeVals_fmTimes[j];
				EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j] = conf.EXPERIMENT_averageTimeVals_fmfuTimes[j];
				EXPERIMENTAL_DATA_VERDICTS[i][j] = conf.EXPERIMENT_verdictsVals[j];
				EXPERIMENTAL_DATA_ISNORMAL[i][j] = conf.EXPERIMENT_isNormalVals[j];
				
				csv.append("Sampling_Chvatal_PerfMTest");
				csv.append(";");
				csv.append(i);
				csv.append(";");
				csv.append(j);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_PVALS[i][j])));//EXPERIMENTAL_DATA_PVALS[i][j]);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_TTESTVALS[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FM[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j])));
				csv.append(";");
				csv.append(String.valueOf((EXPERIMENTAL_DATA_VERDICTS[i][j]==true)?"PASS":"FAIL"));
				csv.append(";");
				csv.append(String.valueOf(EXPERIMENTAL_DATA_ISNORMAL[i][j]));
				csv.append("\n");
			}			
		}
		
		double percFlaky_ChvatalSamplingPerfMTest=  getPercentageOfFlakyTests(EXPERIMENTAL_DATA_VERDICTS);
		double percStatisticalSign_ChvatalSamplingPerfMTest = getPercentageOfStatisticalSignificance(EXPERIMENTAL_DATA_PVALS);
		
		for(int i = 0;i< NUMS_OF_EXPERIMENT_RUNS; i++) {
			Sampling_ICPL_PerfMTest conf = new Sampling_ICPL_PerfMTest();
			
			conf.test();
			
			
			for(int j = 0; j<AbstractFMOperatorPerfMTest.NUMBER_OF_FMs; j++) {
				EXPERIMENTAL_DATA_PVALS[i][j] = conf.EXPERIMENT_pvalVals[j];
				EXPERIMENTAL_DATA_TTESTVALS[i][j] = conf.EXPERIMENT_ttestVals[j];
				EXPERIMENTAL_DATA_AVERAGE_FM[i][j] = conf.EXPERIMENT_averageTimeVals_fmTimes[j];
				EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j] = conf.EXPERIMENT_averageTimeVals_fmfuTimes[j];
				EXPERIMENTAL_DATA_VERDICTS[i][j] = conf.EXPERIMENT_verdictsVals[j];
				EXPERIMENTAL_DATA_ISNORMAL[i][j] = conf.EXPERIMENT_isNormalVals[j];
				
				csv.append("Sampling_ICPL_PerfMTest");
				csv.append(";");
				csv.append(i);
				csv.append(";");
				csv.append(j);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_PVALS[i][j])));//EXPERIMENTAL_DATA_PVALS[i][j]);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_TTESTVALS[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FM[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j])));
				csv.append(";");
				csv.append(String.valueOf((EXPERIMENTAL_DATA_VERDICTS[i][j]==true)?"PASS":"FAIL"));
				csv.append(";");
				csv.append(String.valueOf(EXPERIMENTAL_DATA_ISNORMAL[i][j]));
				csv.append("\n");
			}			
		}
		
		double percFlaky_ICPLSamplingPerfMTest =  getPercentageOfFlakyTests(EXPERIMENTAL_DATA_VERDICTS);
		double percStatisticalSign_ICPLSamplingPerfMTest = getPercentageOfStatisticalSignificance(EXPERIMENTAL_DATA_PVALS);
		
		
		for(int i = 0;i< NUMS_OF_EXPERIMENT_RUNS; i++) {
			Sampling_OneWise_PerfMTest conf = new Sampling_OneWise_PerfMTest();
			
			conf.test();
			
			
			for(int j = 0; j<AbstractFMOperatorPerfMTest.NUMBER_OF_FMs; j++) {
				EXPERIMENTAL_DATA_PVALS[i][j] = conf.EXPERIMENT_pvalVals[j];
				EXPERIMENTAL_DATA_TTESTVALS[i][j] = conf.EXPERIMENT_ttestVals[j];
				EXPERIMENTAL_DATA_AVERAGE_FM[i][j] = conf.EXPERIMENT_averageTimeVals_fmTimes[j];
				EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j] = conf.EXPERIMENT_averageTimeVals_fmfuTimes[j];
				EXPERIMENTAL_DATA_VERDICTS[i][j] = conf.EXPERIMENT_verdictsVals[j];
				EXPERIMENTAL_DATA_ISNORMAL[i][j] = conf.EXPERIMENT_isNormalVals[j];
				
				csv.append("Sampling_OneWise_PerfMTest");
				csv.append(";");
				csv.append(i);
				csv.append(";");
				csv.append(j);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_PVALS[i][j])));//EXPERIMENTAL_DATA_PVALS[i][j]);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_TTESTVALS[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FM[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j])));
				csv.append(";");
				csv.append(String.valueOf((EXPERIMENTAL_DATA_VERDICTS[i][j]==true)?"PASS":"FAIL"));
				csv.append(";");
				csv.append(String.valueOf(EXPERIMENTAL_DATA_ISNORMAL[i][j]));
				csv.append("\n");
			}			
		}
		
		double percFlaky_OneWiseSamplingPerfMTest =  getPercentageOfFlakyTests(EXPERIMENTAL_DATA_VERDICTS);
		double percStatisticalSign_OneWiseSamplingPerfMTest = getPercentageOfStatisticalSignificance(EXPERIMENTAL_DATA_PVALS);
		
		
		
		for(int i = 0;i< NUMS_OF_EXPERIMENT_RUNS; i++) {
			Sampling_PairWiseIncLing_PerfMTest conf = new Sampling_PairWiseIncLing_PerfMTest();
			
			conf.test();
			
			
			for(int j = 0; j<AbstractFMOperatorPerfMTest.NUMBER_OF_FMs; j++) {
				EXPERIMENTAL_DATA_PVALS[i][j] = conf.EXPERIMENT_pvalVals[j];
				EXPERIMENTAL_DATA_TTESTVALS[i][j] = conf.EXPERIMENT_ttestVals[j];
				EXPERIMENTAL_DATA_AVERAGE_FM[i][j] = conf.EXPERIMENT_averageTimeVals_fmTimes[j];
				EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j] = conf.EXPERIMENT_averageTimeVals_fmfuTimes[j];
				EXPERIMENTAL_DATA_VERDICTS[i][j] = conf.EXPERIMENT_verdictsVals[j];
				EXPERIMENTAL_DATA_ISNORMAL[i][j] = conf.EXPERIMENT_isNormalVals[j];
				
				csv.append("Sampling_PairWiseIncLing_PerfMTest");
				csv.append(";");
				csv.append(i);
				csv.append(";");
				csv.append(j);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_PVALS[i][j])));//EXPERIMENTAL_DATA_PVALS[i][j]);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_TTESTVALS[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FM[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j])));
				csv.append(";");
				csv.append(String.valueOf((EXPERIMENTAL_DATA_VERDICTS[i][j]==true)?"PASS":"FAIL"));
				csv.append(";");
				csv.append(String.valueOf(EXPERIMENTAL_DATA_ISNORMAL[i][j]));
				csv.append("\n");
			}			
		}
		
		double percFlaky_PairWiseIncLingSamplingPerfMTest =  getPercentageOfFlakyTests(EXPERIMENTAL_DATA_VERDICTS);
		double percStatisticalSign_PairWiseIncLingSamplingPerfMTest = getPercentageOfStatisticalSignificance(EXPERIMENTAL_DATA_PVALS);
		
		
		
		for(int i = 0;i< NUMS_OF_EXPERIMENT_RUNS; i++) {
			Sampling_PairWiseYasa_PerfMTest conf = new Sampling_PairWiseYasa_PerfMTest();
			
			conf.test();
			
			
			for(int j = 0; j<AbstractFMOperatorPerfMTest.NUMBER_OF_FMs; j++) {
				EXPERIMENTAL_DATA_PVALS[i][j] = conf.EXPERIMENT_pvalVals[j];
				EXPERIMENTAL_DATA_TTESTVALS[i][j] = conf.EXPERIMENT_ttestVals[j];
				EXPERIMENTAL_DATA_AVERAGE_FM[i][j] = conf.EXPERIMENT_averageTimeVals_fmTimes[j];
				EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j] = conf.EXPERIMENT_averageTimeVals_fmfuTimes[j];
				EXPERIMENTAL_DATA_VERDICTS[i][j] = conf.EXPERIMENT_verdictsVals[j];
				EXPERIMENTAL_DATA_ISNORMAL[i][j] = conf.EXPERIMENT_isNormalVals[j];
				
				csv.append("Sampling_PairWiseYasa_PerfMTest");
				csv.append(";");
				csv.append(i);
				csv.append(";");
				csv.append(j);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_PVALS[i][j])));//EXPERIMENTAL_DATA_PVALS[i][j]);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_TTESTVALS[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FM[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j])));
				csv.append(";");
				csv.append(String.valueOf((EXPERIMENTAL_DATA_VERDICTS[i][j]==true)?"PASS":"FAIL"));
				csv.append(";");
				csv.append(String.valueOf(EXPERIMENTAL_DATA_ISNORMAL[i][j]));
				csv.append("\n");
			}			
		}
		
		double percFlaky_PairWiseYasaSamplingPerfMTest =  getPercentageOfFlakyTests(EXPERIMENTAL_DATA_VERDICTS);
		double percStatisticalSign_PairWiseYasaSamplingPerfMTest = getPercentageOfStatisticalSignificance(EXPERIMENTAL_DATA_PVALS);
		
		
		
		for(int i = 0;i< NUMS_OF_EXPERIMENT_RUNS; i++) {
			Sampling_Random_PerfMTest conf = new Sampling_Random_PerfMTest();
			
			conf.test();
			
			
			for(int j = 0; j<AbstractFMOperatorPerfMTest.NUMBER_OF_FMs; j++) {
				EXPERIMENTAL_DATA_PVALS[i][j] = conf.EXPERIMENT_pvalVals[j];
				EXPERIMENTAL_DATA_TTESTVALS[i][j] = conf.EXPERIMENT_ttestVals[j];
				EXPERIMENTAL_DATA_AVERAGE_FM[i][j] = conf.EXPERIMENT_averageTimeVals_fmTimes[j];
				EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j] = conf.EXPERIMENT_averageTimeVals_fmfuTimes[j];
				EXPERIMENTAL_DATA_VERDICTS[i][j] = conf.EXPERIMENT_verdictsVals[j];
				EXPERIMENTAL_DATA_ISNORMAL[i][j] = conf.EXPERIMENT_isNormalVals[j];
				
				csv.append("Sampling_Random_PerfMTest");
				csv.append(";");
				csv.append(i);
				csv.append(";");
				csv.append(j);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_PVALS[i][j])));//EXPERIMENTAL_DATA_PVALS[i][j]);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_TTESTVALS[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FM[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j])));
				csv.append(";");
				csv.append(String.valueOf((EXPERIMENTAL_DATA_VERDICTS[i][j]==true)?"PASS":"FAIL"));
				csv.append(";");
				csv.append(String.valueOf(EXPERIMENTAL_DATA_ISNORMAL[i][j]));
				csv.append("\n");
			}			
		}
		
		double percFlaky_RandomSamplingPerfMTest =  getPercentageOfFlakyTests(EXPERIMENTAL_DATA_VERDICTS);
		double percStatisticalSign_RandomSamplingPerfMTest = getPercentageOfStatisticalSignificance(EXPERIMENTAL_DATA_PVALS);
		
		
		
		for(int i = 0;i< NUMS_OF_EXPERIMENT_RUNS; i++) {
			Sampling_ThreeWiseYasa_PerfMTest conf = new Sampling_ThreeWiseYasa_PerfMTest();
			
			conf.test();
			
			
			for(int j = 0; j<AbstractFMOperatorPerfMTest.NUMBER_OF_FMs; j++) {
				EXPERIMENTAL_DATA_PVALS[i][j] = conf.EXPERIMENT_pvalVals[j];
				EXPERIMENTAL_DATA_TTESTVALS[i][j] = conf.EXPERIMENT_ttestVals[j];
				EXPERIMENTAL_DATA_AVERAGE_FM[i][j] = conf.EXPERIMENT_averageTimeVals_fmTimes[j];
				EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j] = conf.EXPERIMENT_averageTimeVals_fmfuTimes[j];
				EXPERIMENTAL_DATA_VERDICTS[i][j] = conf.EXPERIMENT_verdictsVals[j];
				EXPERIMENTAL_DATA_ISNORMAL[i][j] = conf.EXPERIMENT_isNormalVals[j];
				
				csv.append("Sampling_ThreeWiseYasa_PerfMTest");
				csv.append(";");
				csv.append(i);
				csv.append(";");
				csv.append(j);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_PVALS[i][j])));//EXPERIMENTAL_DATA_PVALS[i][j]);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_TTESTVALS[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FM[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j])));
				csv.append(";");
				csv.append(String.valueOf((EXPERIMENTAL_DATA_VERDICTS[i][j]==true)?"PASS":"FAIL"));
				csv.append(";");
				csv.append(String.valueOf(EXPERIMENTAL_DATA_ISNORMAL[i][j]));
				csv.append("\n");
			}			
		}
		
		double percFlaky_ThreeWiseYasaSamplingPerfMTest =  getPercentageOfFlakyTests(EXPERIMENTAL_DATA_VERDICTS);
		double percStatisticalSign_ThreeWiseYasaSamplingPerfMTest = getPercentageOfStatisticalSignificance(EXPERIMENTAL_DATA_PVALS);
		
		
		for(int i = 0;i< NUMS_OF_EXPERIMENT_RUNS; i++) {
			T_Chvatal_PerfMTest conf = new T_Chvatal_PerfMTest();
			
			conf.test();
			
			
			for(int j = 0; j<AbstractFMOperatorPerfMTest.NUMBER_OF_FMs; j++) {
				EXPERIMENTAL_DATA_PVALS[i][j] = conf.EXPERIMENT_pvalVals[j];
				EXPERIMENTAL_DATA_TTESTVALS[i][j] = conf.EXPERIMENT_ttestVals[j];
				EXPERIMENTAL_DATA_AVERAGE_FM[i][j] = conf.EXPERIMENT_averageTimeVals_fmTimes[j];
				EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j] = conf.EXPERIMENT_averageTimeVals_fmfuTimes[j];
				EXPERIMENTAL_DATA_VERDICTS[i][j] = conf.EXPERIMENT_verdictsVals[j];
				EXPERIMENTAL_DATA_ISNORMAL[i][j] = conf.EXPERIMENT_isNormalVals[j];
				
				csv.append("T_Chvatal_PerfMTest");
				csv.append(";");
				csv.append(i);
				csv.append(";");
				csv.append(j);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_PVALS[i][j])));//EXPERIMENTAL_DATA_PVALS[i][j]);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_TTESTVALS[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FM[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j])));
				csv.append(";");
				csv.append(String.valueOf((EXPERIMENTAL_DATA_VERDICTS[i][j]==true)?"PASS":"FAIL"));
				csv.append(";");
				csv.append(String.valueOf(EXPERIMENTAL_DATA_ISNORMAL[i][j]));
				csv.append("\n");
			}			
		}
		
		double percFlaky_T_Chvatal_PerfMTest =  getPercentageOfFlakyTests(EXPERIMENTAL_DATA_VERDICTS);
		double percStatisticalSign_T_Chvatal_PerfMTest = getPercentageOfStatisticalSignificance(EXPERIMENTAL_DATA_PVALS);
		
		
		
		
		
		for(int i = 0;i< NUMS_OF_EXPERIMENT_RUNS; i++) {
			T_ICPL_PerfMTest conf = new T_ICPL_PerfMTest();
			
			conf.test();
			
			
			for(int j = 0; j<AbstractFMOperatorPerfMTest.NUMBER_OF_FMs; j++) {
				EXPERIMENTAL_DATA_PVALS[i][j] = conf.EXPERIMENT_pvalVals[j];
				EXPERIMENTAL_DATA_TTESTVALS[i][j] = conf.EXPERIMENT_ttestVals[j];
				EXPERIMENTAL_DATA_AVERAGE_FM[i][j] = conf.EXPERIMENT_averageTimeVals_fmTimes[j];
				EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j] = conf.EXPERIMENT_averageTimeVals_fmfuTimes[j];
				EXPERIMENTAL_DATA_VERDICTS[i][j] = conf.EXPERIMENT_verdictsVals[j];
				EXPERIMENTAL_DATA_ISNORMAL[i][j] = conf.EXPERIMENT_isNormalVals[j];
				
				csv.append("T_ICPL_PerfMTest");
				csv.append(";");
				csv.append(i);
				csv.append(";");
				csv.append(j);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_PVALS[i][j])));//EXPERIMENTAL_DATA_PVALS[i][j]);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_TTESTVALS[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FM[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j])));
				csv.append(";");
				csv.append(String.valueOf((EXPERIMENTAL_DATA_VERDICTS[i][j]==true)?"PASS":"FAIL"));
				csv.append(";");
				csv.append(String.valueOf(EXPERIMENTAL_DATA_ISNORMAL[i][j]));
				csv.append("\n");
			}			
		}
		
		double percFlaky_T_ICPL_PerfMTest =  getPercentageOfFlakyTests(EXPERIMENTAL_DATA_VERDICTS);
		double percStatisticalSign_T_ICPL_PerfMTest = getPercentageOfStatisticalSignificance(EXPERIMENTAL_DATA_PVALS);
		
		
		
		
		
		for(int i = 0;i< NUMS_OF_EXPERIMENT_RUNS; i++) {
			T_Yasa_PerfMTest conf = new T_Yasa_PerfMTest();
			
			conf.test();
			
			
			for(int j = 0; j<AbstractFMOperatorPerfMTest.NUMBER_OF_FMs; j++) {
				EXPERIMENTAL_DATA_PVALS[i][j] = conf.EXPERIMENT_pvalVals[j];
				EXPERIMENTAL_DATA_TTESTVALS[i][j] = conf.EXPERIMENT_ttestVals[j];
				EXPERIMENTAL_DATA_AVERAGE_FM[i][j] = conf.EXPERIMENT_averageTimeVals_fmTimes[j];
				EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j] = conf.EXPERIMENT_averageTimeVals_fmfuTimes[j];
				EXPERIMENTAL_DATA_VERDICTS[i][j] = conf.EXPERIMENT_verdictsVals[j];
				EXPERIMENTAL_DATA_ISNORMAL[i][j] = conf.EXPERIMENT_isNormalVals[j];
				
				csv.append("T_Yasa_PerfMTest");
				csv.append(";");
				csv.append(i);
				csv.append(";");
				csv.append(j);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_PVALS[i][j])));//EXPERIMENTAL_DATA_PVALS[i][j]);
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_TTESTVALS[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FM[i][j])));
				csv.append(";");
				csv.append(String.valueOf(df3.format(EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j])));
				csv.append(";");
				csv.append(String.valueOf((EXPERIMENTAL_DATA_VERDICTS[i][j]==true)?"PASS":"FAIL"));
				csv.append(";");
				csv.append(String.valueOf(EXPERIMENTAL_DATA_ISNORMAL[i][j]));
				csv.append("\n");
			}			
		}
		
		double percFlaky_T_Yasa_PerfMTest =  getPercentageOfFlakyTests(EXPERIMENTAL_DATA_VERDICTS);
		double percStatisticalSign_T_Yasa_PerfMTest = getPercentageOfStatisticalSignificance(EXPERIMENTAL_DATA_PVALS);
		
		
		File output = new File("output/EXPERIMENTS_SPLC2020");// + this.getClass().getSimpleName());
		if (!output.exists()) {
			output.mkdirs();
		}
		File newFile = new File(output, "samplingExperiments.csv");
		Utils.writeStringToFile(newFile, csv.toString());
		
		
		System.out.println("----------------------------------------------------------");
		System.out.println("----------------------------------------------------------");
		
		System.out.println("percStatisticalSign_AllConfigurationsSamplingPerfMTest = " + percStatisticalSign_AllConfigurationsSamplingPerfMTest);
		System.out.println("percStatisticalSign_ChvatalSamplingPerfMTest = " + percStatisticalSign_ChvatalSamplingPerfMTest);
		System.out.println("percStatisticalSign_ICPLSamplingPerfMTest = " + percStatisticalSign_ICPLSamplingPerfMTest);
		System.out.println("percStatisticalSign_OneWiseSamplingPerfMTest = " + percStatisticalSign_OneWiseSamplingPerfMTest);
		System.out.println("percStatisticalSign_PairWiseIncLingSamplingPerfMTest = " +percStatisticalSign_PairWiseIncLingSamplingPerfMTest);
		System.out.println("percStatisticalSign_PairWiseYasaSamplingPerfMTest = " +percStatisticalSign_PairWiseYasaSamplingPerfMTest);
		System.out.println("percStatisticalSign_RandomSamplingPerfMTest = " +percStatisticalSign_RandomSamplingPerfMTest);
		System.out.println("percStatisticalSign_ThreeWiseYasaSamplingPerfMTest = " +percStatisticalSign_ThreeWiseYasaSamplingPerfMTest);
		System.out.println("percStatisticalSign_T_Chvatal_PerfMTest = " + percStatisticalSign_T_Chvatal_PerfMTest);
		System.out.println("percStatisticalSign_T_ICPL_PerfMTest = " + percStatisticalSign_T_ICPL_PerfMTest);
		System.out.println("percStatisticalSign_T_Yasa_PerfMTest = " + percStatisticalSign_T_Yasa_PerfMTest);

		
		
		
		System.out.println("----------------------------------------------------------");
		System.out.println("----------------------------------------------------------");
		System.out.println("PercentageFlaky_AllConfigurationsSampling = " + percFlaky_AllConfigurationsSamplingPerfMTest);
		System.out.println("PercentageFlaky_ChvatalSamplingPerfMTest = " + percFlaky_ChvatalSamplingPerfMTest);
		System.out.println("PercentageFlaky_ICPLSamplingPerfMTest = " + percFlaky_ICPLSamplingPerfMTest);
		System.out.println("PercentageFlaky_OneWiseSamplingPerfMTest = " + percFlaky_OneWiseSamplingPerfMTest);
		System.out.println("PercentageFlaky_PairWiseIncLingSamplingPerfMTest = " + percFlaky_PairWiseIncLingSamplingPerfMTest);
		System.out.println("PercentageFlaky_PairWiseYasaSamplingPerfMTest = " + percFlaky_PairWiseYasaSamplingPerfMTest);
		System.out.println("PercentageFlaky_RandomSamplingPerfMTest = " + percFlaky_RandomSamplingPerfMTest);
		System.out.println("PercentageFlaky_ThreeWiseYasaSamplingPerfMTest = " + percFlaky_ThreeWiseYasaSamplingPerfMTest);
		System.out.println("percFlaky_T_Chvatal_PerfMTest = " + percFlaky_T_Chvatal_PerfMTest);
		System.out.println("percFlaky_T_ICPL_PerfMTest = " + percFlaky_T_ICPL_PerfMTest);
		System.out.println("percFlaky_T_Yasa_PerfMTest = " + percFlaky_T_Yasa_PerfMTest);


		

		
		
		
	}
	
	
	static double getPercentageOfStatisticalSignificance(double[][] pvals) {
		int numsOfStatisticalSignificanceTests = 0;
		for(int i = 0; i<pvals.length;i++) {
			for(int j=0;j<pvals[0].length;j++) {
				if(pvals[i][j]<0.05)
					numsOfStatisticalSignificanceTests++;
					
			}
		}
		//System.out.println("numsOfStatisticalSignificanceTests = " + numsOfStatisticalSignificanceTests);
		return (double)numsOfStatisticalSignificanceTests/(double)(pvals[0].length*pvals.length);
	}
	
	static double getPercentageOfFlakyTests(boolean[][] verdicts) {
		//verdicts[0].length
		int numsofFlakyTests = 0;
		
		for(int testCase = 0; testCase<verdicts[0].length;testCase++) {
				
			int numsOfFails = 0;
			int numsOfPass = 0;
			for(int run = 0; run<verdicts.length;run++) {
				if(verdicts[run][testCase]==true) {
					numsOfPass++;
				}
				else {
					numsOfFails++;
				}
			}
			if(numsOfFails>0&&numsOfPass>0)
				numsofFlakyTests++;
				
		}
		return ((double)numsofFlakyTests/(double)verdicts[0].length);
	}

}
