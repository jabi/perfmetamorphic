package de.ovgu.featureide.fm.core_perftest;

import de.ovgu.featureide.fm.core.base.IFeatureModel;

public abstract class AbstractTWisePerfMTest extends AbstractFMOperatorPerfMTest {

	public int t;
	
	@Override
	public IFeatureModel getNextFM(long randomSeed) {
		t = 2;
		return super.getNextFM(randomSeed);
	}
	
	@Override
	/**
	 * We use the same fm but we change the t of the algorithm
	 */
	public IFeatureModel getFollowUpFM(long randomSeed, IFeatureModel fm) {
		t = 3;
		return fm.clone();
	}

}
