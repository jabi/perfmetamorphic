package de.ovgu.featureide.fm.core_perftest;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import de.ovgu.featureide.fm.core.base.IFeature;
import de.ovgu.featureide.fm.core.base.IFeatureModel;
import de.ovgu.featureide.fm.core.base.IFeatureModelFactory;
import de.ovgu.featureide.fm.core.base.impl.FMFactoryManager;
import de.ovgu.featureide.fm.core.functional.Functional;

public class NeighbourFMGenerator {
	
	/**
	 * It randomly selects a parent to create a child feature. This feature will be
	 * optional, but if the parent is an OR group, or an Alternative group, the
	 * child will be an option in the OR or in the Alternative.
	 */
	public static final String GENERALIZATION_ADD_OPTIONAL = "GENERALIZATION_ADD_OPTIONAL";
	
	/**
	 * It randomly selects a constraint and removes it from the fm
	 */
	public static final String GENERALIZATION_REMOVE_CONSTRAINT = "GENERALIZATION_REMOVE_CONSTRAINT";
	
	public static IFeatureModel createNeighbourFM(long randomSeed, IFeatureModel initialFM, String neighbourType) {
		Random random = new Random(randomSeed);
		IFeatureModel neighbourFM = initialFM.clone();
		
		final IFeatureModelFactory factory = FMFactoryManager.getInstance().getFactory(neighbourFM);

		if (neighbourType == GENERALIZATION_ADD_OPTIONAL) {
			IFeature newFeature = factory.createFeature(neighbourFM, "NEW_FEATURE");
			newFeature.getStructure().setMandatory(false);

			// Shuffle the list of features
			final List<IFeature> list = new LinkedList<>(Functional.toList(neighbourFM.getFeatures()));
			final List<IFeature> randomizedList = new LinkedList<>();
			while (!list.isEmpty()) {
				randomizedList.add(list.remove(random.nextInt(list.size())));
			}

			// Get first
			IFeature feature = randomizedList.get(0);

			// Add optional
			feature.getStructure().addChild(newFeature.getStructure());
			newFeature.getStructure().setParent(feature.getStructure());
			neighbourFM.addFeature(newFeature);
			
		} if (neighbourType == GENERALIZATION_REMOVE_CONSTRAINT) {
			// Get random constraint
			int constraints = neighbourFM.getConstraintCount();
			if(constraints > 0) {
				int randomConstraintsIndex = random.nextInt(constraints);
				// Remove it
				neighbourFM.removeConstraint(randomConstraintsIndex);
			}
		}
		return neighbourFM;
	}
	
}
