package de.ovgu.featureide.fm.core_perftest;

import java.io.File;

import de.ovgu.featureide.fm.core.base.IFeatureModel;

public abstract class AbstractFMOperatorRealFMPerfMTest extends AbstractFMOperatorPerfMTest {

	int i = 0;
	
	@Override
	public IFeatureModel getNextFM(long randomSeed) {
		File fmsFolder = new File("fms");
		File[] file = fmsFolder.listFiles();
		IFeatureModel fm = Utils.load(file[i]);
		i++;
		return fm;
	}
	
}
