package de.ovgu.featureide.fm.core_perftest;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.apache.commons.math3.stat.inference.MannWhitneyUTest;
// import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
// import org.apache.commons.math3.stat.inference.ChiSquareTest;
import org.apache.commons.math3.stat.inference.TTest;
import org.junit.Assert;
import org.junit.Test;

import de.ovgu.featureide.fm.core.base.IFeatureModel;
// import org.apache.commons.math3.distribution.NormalDistribution;
import flanagan.analysis.Normality;

public abstract class AbstractFMOperatorPerfMTest {

	public static int NUMBER_OF_FMs = 1;
	public static int NUMBER_OF_FEATURES = 10;
	public static int NUMBER_OF_REPETITIONS = 100;
	public static long RANDOM_SEED = 1;

	public long[] EXPERIMENT_averageTimeVals_fmTimes = new long[NUMBER_OF_FMs];
	public long[] EXPERIMENT_averageTimeVals_fmfuTimes = new long[NUMBER_OF_FMs];
	public double[] EXPERIMENT_ttestVals = new double[NUMBER_OF_FMs];
	public double[] EXPERIMENT_pvalVals = new double[NUMBER_OF_FMs];
	public boolean[] EXPERIMENT_verdictsVals = new boolean[NUMBER_OF_FMs]; // true = pass; false = fail
	public boolean[] EXPERIMENT_isNormalVals = new boolean[NUMBER_OF_FMs];

	public abstract void prepareOperator(IFeatureModel fm);

	public abstract void executeOperator();

	int i = 8;

	/**
	 * To be overridden if other random generator or fm provider is used
	 * 
	 * @param randomSeed
	 * @return
	 */
	public IFeatureModel getNextFM(long randomSeed) {
		File fmsFolder = new File("random_fms");
		File[] file = fmsFolder.listFiles();
		IFeatureModel fm = Utils.loadSXFM(file[i]);
		//i++;
		return fm;
	}

	/**
	 * To be overridden if other neighbour fm strategy is used
	 * 
	 * @param fm
	 * @param randomSeed
	 * 
	 * @return
	 */
	public IFeatureModel getFollowUpFM(long randomSeed, IFeatureModel fm) {
		return NeighbourFMGenerator.createNeighbourFM(randomSeed, fm, NeighbourFMGenerator.GENERALIZATION_ADD_OPTIONAL);
	}

	/**
	 * To be overridden if we cannot make the assumption of normality. whether you
	 * assume that the performance results should follow a normal distribution
	 * (e.g., it has no stochastic component affecting performance).
	 * 
	 * @return
	 */
	public static boolean getAssumptionOfNormality() {
		return true;
	}

	@Test
	public void test() {

		long randomSeed = RANDOM_SEED;
		//i = 0;

		// Main loop for each fm and follow-up fm
		for (int fmNumber = 0; fmNumber < NUMBER_OF_FMs; fmNumber++) {

			double[] fmTimes = new double[NUMBER_OF_REPETITIONS];
			double[] fmfuTimes = new double[NUMBER_OF_REPETITIONS];

			IFeatureModel fm = getNextFM(randomSeed);
			// System.out.println(fm);

			// Update the randomSeed for the next fm
			randomSeed++;

			// First execution is not added to remove warming-up effect
			for (int repetitionNumber = 0; repetitionNumber < NUMBER_OF_REPETITIONS + 1; repetitionNumber++) {
				long fmTime = getTime(fm);
				if (repetitionNumber != 0) {
					fmTimes[repetitionNumber - 1] = (double) fmTime;
				}
			}

			IFeatureModel followupFM = getFollowUpFM(randomSeed, fm);
			// System.out.println(followupFM);

			// First execution is not added to remove warming-up effect
			for (int repetitionNumber = 0; repetitionNumber < NUMBER_OF_REPETITIONS + 1; repetitionNumber++) {
				long fmfuTime = getTime(followupFM);
				if (repetitionNumber != 0) {
					fmfuTimes[repetitionNumber - 1] = (double) fmfuTime;
				}
			}

			/**
			 * Analysis of the results. Each FM is a test case, to be repeated N times.
			 * ASSERT must be done at this level
			 */
			StringBuilder csv = new StringBuilder();

			for (int i = 0; i < fmTimes.length; i++) {
				csv.append(fmTimes[i]);
				csv.append(",");
				csv.append(fmfuTimes[i]);
				csv.append("\n");
				// System.out.println(fmTimes.get(i) + "," + fmfuTimes.get(i));
			}

			csv.append("\nAverage\n");
			csv.append(calculateAverage(fmTimes));
			csv.append(",");
			csv.append(calculateAverage(fmfuTimes));

			this.EXPERIMENT_averageTimeVals_fmTimes[fmNumber] = calculateAverage(fmTimes);
			this.EXPERIMENT_averageTimeVals_fmfuTimes[fmNumber] = calculateAverage(fmfuTimes);

			boolean isNormalFM = isNormallyDistributed(fmTimes);
			boolean isNormalFMFU = isNormallyDistributed(fmfuTimes);

			if (!isNormalFM || !isNormalFMFU) {
				this.EXPERIMENT_isNormalVals[fmNumber] = false;
			} else {
				this.EXPERIMENT_isNormalVals[fmNumber] = true;
			}

			if (getAssumptionOfNormality()) {
				if (isNormalFM && !isNormalFMFU) {
					// Assert.fail(
					csv.append(
							"\nWarning: The performance of the initial test was normally distributed but not the one from the follow-up test");
				}
				if (!isNormalFM && isNormalFMFU) {
					// Assert.fail(
					csv.append(
							"\nWarning: The performance of the follow-up test was normally distributed but not the one of the initial test");
				}
				if (!isNormalFM && !isNormalFMFU) {
					// Assert.fail(
					csv.append(
							"\nWarning: The performance of the initial test and the follow-up tests are not normally distributed");
				}
			}

			double pairedTTestVal = 0;
			double pval = 0;

			TTest tt = new TTest();
			if (getAssumptionOfNormality()) {
				pairedTTestVal = tt.pairedT(fmTimes, fmfuTimes);
				pval = tt.pairedTTest(fmTimes, fmfuTimes);
			} else {
				// Normally distributed -> ttest
				MannWhitneyUTest mn = new MannWhitneyUTest();
				pval = mn.mannWhitneyU(fmTimes, fmfuTimes);
				pairedTTestVal = tt.pairedT(fmTimes, fmfuTimes);
			}

			csv.append("\nPaired ttest = " + pairedTTestVal);
			csv.append("\np-val = " + pval);

			this.EXPERIMENT_ttestVals[fmNumber] = pairedTTestVal;
			this.EXPERIMENT_pvalVals[fmNumber] = pval;

			if (pval >= 0.05) {
				// assertTrue(true);
			
				this.EXPERIMENT_verdictsVals[fmNumber] = true;
			} else {
				// Negative values indicate that fmTimesD is lower
				this.EXPERIMENT_verdictsVals[fmNumber] = pairedTTestVal < 0;
			}

			// Assertion
			// Better with statistical analysis
			// assertTrue(calculateAverage(fmTimes) <= calculateAverage(fmfuTimes));

			// Save the file
			File output = new File("output/" + this.getClass().getSimpleName());
			if (!output.exists()) {
				output.mkdirs();
			}

			// Save fm and follow up fm to file
//			File newFileFM = new File(output, "fm" + fmNumber + ".xml");
//			File newFileFMFU = new File(output, "fm" + fmNumber + "_fu.xml");
//			Utils.saveFM(newFileFM, fm);
//			Utils.saveFM(newFileFMFU, followupFM);

			File newFile = new File(output, "fm" + fmNumber + ".txt");
			Utils.writeStringToFile(newFile, csv.toString());

		}

	}

	/**
	 * Prepare the operator and get the time of its execution
	 * 
	 * @param fm
	 * @return
	 */
	private long getTime(IFeatureModel fm) {
		prepareOperator(fm);
		long start = System.currentTimeMillis();
		executeOperator();
		long elapsedTime = System.currentTimeMillis() - start;
		return elapsedTime;
	}

	/**
	 * Check whether a sample is normally distributed
	 * 
	 * @param sample
	 * @return true if normally distributed
	 */
	private static boolean isNormallyDistributed(double[] sample) {
		Normality nrm_sample = new Normality(sample);
		if (nrm_sample.shapiroWilkPvalue() < 0.05) {
			return true;
		}
		return false;
	}

	/**
	 * Calculate the average of a set of double values
	 * 
	 * @param values
	 * @return average
	 */
	private static long calculateAverage(double[] vals) {
		long sum = 0;
		for (double v : vals) {
			sum += v;
		}
		return sum / vals.length;
	}

}
