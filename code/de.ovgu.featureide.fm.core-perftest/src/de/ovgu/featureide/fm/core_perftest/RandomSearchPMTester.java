package de.ovgu.featureide.fm.core_perftest;





import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.ovgu.featureide.fm.core.AtomicSetsFMPerfMTest;
import de.ovgu.featureide.fm.core.CoreFeaturesPerfMTest;
import de.ovgu.featureide.fm.core.CountSolutionsPerfMTest;
import de.ovgu.featureide.fm.core.FalseOptionalFeaturesPerfMTest;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.Sampling_PairWiseIncLing_PerfMTest;
import de.ovgu.featureide.fm.core.base.IConstraint;
import de.ovgu.featureide.fm.core.base.IFeatureModel;
import de.ovgu.featureide.fm.core.base.impl.Feature;
import de.ovgu.featureide.fm.core.io.manager.FeatureModelManager;
import es.us.isa.FAMA.models.FAMAfeatureModel.FAMAFeatureModel;
import es.us.isa.generator.FM.AbstractFMGenerator;
import es.us.isa.generator.FM.FMGenerator;
import es.us.isa.generator.FM.GeneratorCharacteristics;
import es.us.isa.utils.FMWriter;
import randomfms.GetFMsWithoutDeadFalseOptionalRedundant;
import randomfms.PrepareToReadInFeatureIDE;
import randomfms.ValidFMsWithBeTTy;


public class RandomSearchPMTester {
	
	File temp = new File("temp");
	File resultFolder = new File("result");
	
	int NUMBER_OF_FEATURES = 500;
	int NUMBER_OF_CONSTRAINTS = 30;
	int NUMBER_OF_RUNS = 50;
	
	public void launchRandomPMTest() {

		/*FAMAFeatureModel fm = generateRandomFM(1000, 1, 1);
		Sampling_PairWiseIncLing_PerfMTest sampling = new Sampling_PairWiseIncLing_PerfMTest();*/
		
		//int numOfFeatures = 2500;
		int worstCaseSeed = 0;
		double timeDiference = 10000;
		
		for(int i=0;i<NUMBER_OF_RUNS;i++) {
			System.out.println("RUN NUMBER = " + i);
			double dif = executeTest(i);
			if(dif<timeDiference) {
				timeDiference = dif;// = timeDiference;
				worstCaseSeed = i;
			}
			
		}
		
		//IFeatureModel fm2 = Utils.load("C:\\Dropbox (MGEP)\\RepositoriosGitHub\\ColaboracionJabierMartinez\\fms\\fm_1000_1.xml");
		System.out.println("worst case time difference = " + timeDiference);
		System.out.println("worst case seed = " + worstCaseSeed);
	
		System.out.println("Aitor");

	}
	
	private double executeTest(int seed) {
		
		File output = new File(temp, "fms_f" + NUMBER_OF_FEATURES + "_c" + NUMBER_OF_CONSTRAINTS);
		ValidFMsWithBeTTy.generateValidFMsWithBeTTy(output, seed, 100, NUMBER_OF_FEATURES, NUMBER_OF_CONSTRAINTS);
		
		//File output = new File(temp, "fms_f" + 1000 + "_c" + 1);
		File fixed = new File(output.getParentFile(), output.getName() + "_fixed");
		PrepareToReadInFeatureIDE.prepareToReadInFeatureIDE(output, fixed);

		// Get the first correct one
		//File result = new File(resultFolder, "fm_f" + f + "_c" + c + ".xml");
		//GetFMsWithoutDeadFalseOptionalRedundant.getFMWithoutDeadFalseOptionalRedundant(fixed, result);
		
		
		File fmFile = GetFMsWithoutDeadFalseOptionalRedundant.getFMWithoutDeadFalseOptionalRedundant(fixed);
		
		IFeatureModel fm = Utils.loadSXFM(fmFile);
		
		IFeatureModel followUp = getFollowUpFM(fmFile);
		
		CountSolutionsPerfMTest cfp = new CountSolutionsPerfMTest();
		cfp.prepareOperator(fm);
		
		
		
		
		double avgTimeSource = 0;
		double maxTimeSource = 0;
		for(int i = 0;i<10000+1;i++) {
			double start = System.currentTimeMillis();
			cfp.executeOperator();
			double elapsedTime = System.currentTimeMillis() - start;
			if(i!=0) {
				avgTimeSource += elapsedTime;
				if(elapsedTime > maxTimeSource)
					maxTimeSource = elapsedTime;
			}
		}
		avgTimeSource = avgTimeSource / 10000;
		
		double avgTimeFolloUP = 0;
		double maxTimeFollowUP= 0;
		cfp.prepareOperator(followUp);
		for(int i = 0;i<10000+1;i++) {
			double start = System.currentTimeMillis();
			cfp.executeOperator();
			double elapsedTime = System.currentTimeMillis() - start;
			if(i!=0) {
				avgTimeFolloUP+= elapsedTime;
				if(elapsedTime > maxTimeFollowUP)
					maxTimeFollowUP = elapsedTime;
			}
		}
		avgTimeFolloUP = avgTimeFolloUP /10000;
		
		System.out.println("Follow Up Avg = " + avgTimeFolloUP);
		System.out.println("Source Avg = " + avgTimeSource);
		System.out.println("Follow Up max = " + maxTimeFollowUP);
		System.out.println("Source max = " + maxTimeSource);
		return avgTimeSource-avgTimeFolloUP;
		
	}
	
	private IFeatureModel getFollowUpFM2(File fmFile) {
		IFeatureModel fmNoConstraints = Utils.loadSXFM(fmFile);
		
		
		
		return fmNoConstraints;
	}
	
	/**
	 * 
	 * @param fmFile
	 * @return The same feature model without constraints
	 */
	private IFeatureModel getFollowUpFM(File fmFile) {
		IFeatureModel fmNoConstraints = Utils.loadSXFM(fmFile);
		
		
		while(fmNoConstraints.getConstraints().size()>0) {
			fmNoConstraints.removeConstraint(fmNoConstraints.getConstraints().get(0));
		}
		return fmNoConstraints;
	}

	private static FAMAFeatureModel generateRandomFM(int numberOfFeatures, float ctcPercentage, int seed) {
		GeneratorCharacteristics characteristics = new GeneratorCharacteristics();
		
		characteristics.setNumberOfFeatures(numberOfFeatures); // Number of features
		
		characteristics.setPercentageCTC(ctcPercentage); // Percentage of cross-tree constraints.
		characteristics.setSeed(seed);

		// STEP 2: Generate the model with the specific characteristics (FaMa FM
		// metamodel is used)
		AbstractFMGenerator generator = new FMGenerator();
		FAMAFeatureModel fm = (FAMAFeatureModel) generator.generateFM(characteristics);

		return fm;
		
	}

}
