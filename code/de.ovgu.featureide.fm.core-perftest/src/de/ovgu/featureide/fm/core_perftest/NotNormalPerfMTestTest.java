package de.ovgu.featureide.fm.core_perftest;

import java.util.Random;

import de.ovgu.featureide.fm.core.base.IFeatureModel;

public class NotNormalPerfMTestTest extends AbstractFMOperatorPerfMTest{

	@Override
	public void prepareOperator(IFeatureModel fm) {
		
	}

	@Override
	public void executeOperator() {
		Random random = new Random();
		double rd = random.nextDouble();
		Double duration = 200 * rd;
	    try {
			Thread.sleep(duration.longValue());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
