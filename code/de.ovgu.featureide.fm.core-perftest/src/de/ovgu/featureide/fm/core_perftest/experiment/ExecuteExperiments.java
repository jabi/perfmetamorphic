package de.ovgu.featureide.fm.core_perftest.experiment;

import java.io.File;
import java.text.DecimalFormat;

import de.ovgu.featureide.fm.core.AtomicSetsFMPerfMTest;
import de.ovgu.featureide.fm.core.CalculateRedundantConstraintsPerfMTest;
import de.ovgu.featureide.fm.core.CoreFeaturesPerfMTest;
import de.ovgu.featureide.fm.core.CountSolutionsPerfMTest;
import de.ovgu.featureide.fm.core.DeadFeaturesPerfMTest;
import de.ovgu.featureide.fm.core.FalseOptionalFeaturesPerfMTest;
import de.ovgu.featureide.fm.core.ValidFMPerfMTest;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.Sampling_AllConfigurations_PerfMTest;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.Sampling_Chvatal_PerfMTest;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.Sampling_ICPL_PerfMTest;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.Sampling_OneWise_PerfMTest;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.Sampling_PairWiseIncLing_PerfMTest;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.Sampling_PairWiseYasa_PerfMTest;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.Sampling_Random_PerfMTest;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.Sampling_ThreeWiseYasa_PerfMTest;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.T_Chvatal_PerfMTest;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.T_ICPL_PerfMTest;
import de.ovgu.featureide.fm.core.analysis.cnf.generator.configuration.T_Yasa_PerfMTest;
import de.ovgu.featureide.fm.core_perftest.AbstractFMOperatorPerfMTest;
import de.ovgu.featureide.fm.core_perftest.RandomSearchPMTester;
import de.ovgu.featureide.fm.core_perftest.Utils;

public class ExecuteExperiments {

	static int NUMS_OF_EXPERIMENT_RUNS = 100;
	private static DecimalFormat df3 = new DecimalFormat("#.###");

	/**
	 * Main method
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		// Add here the operators to use
		AbstractFMOperatorPerfMTest[] operators = new AbstractFMOperatorPerfMTest[] {

				// Reasoners

				//new AtomicSetsFMPerfMTest(), new CalculateRedundantConstraintsPerfMTest(), new CoreFeaturesPerfMTest(),
				//new CountSolutionsPerfMTest(), new DeadFeaturesPerfMTest(), new FalseOptionalFeaturesPerfMTest(),
				//new ValidFMPerfMTest(),

				// Sampling
				//new Sampling_AllConfigurations_PerfMTest(), 
				//new Sampling_Chvatal_PerfMTest(),
				//new Sampling_ICPL_PerfMTest(), new Sampling_OneWise_PerfMTest(),
				new Sampling_PairWiseIncLing_PerfMTest()//, new Sampling_PairWiseYasa_PerfMTest(),
				//new Sampling_Random_PerfMTest(), 
				//new Sampling_ThreeWiseYasa_PerfMTest(),

				// TWise
				//new T_Chvatal_PerfMTest(), new T_ICPL_PerfMTest(), new T_Yasa_PerfMTest() 
				};

				//new AtomicSetsFMPerfMTest(), new CalculateRedundantConstraintsPerfMTest(), new CoreFeaturesPerfMTest(),
				//new CountSolutionsPerfMTest(), new DeadFeaturesPerfMTest(),
				//new FalseOptionalFeaturesPerfMTest()
				//new ValidFMPerfMTest(),

//				// Sampling
//				//new Sampling_AllConfigurations_PerfMTest(),
//				new Sampling_Chvatal_PerfMTest(),
//				new Sampling_ICPL_PerfMTest(), new Sampling_OneWise_PerfMTest(),
//				new Sampling_PairWiseIncLing_PerfMTest(), new Sampling_PairWiseYasa_PerfMTest(),
//				// new Sampling_Random_PerfMTest(),
//				new Sampling_ThreeWiseYasa_PerfMTest(),
//
//				// TWise
//				new T_Chvatal_PerfMTest(), new T_ICPL_PerfMTest(), new T_Yasa_PerfMTest()
				//};


		RandomSearchPMTester rs = new RandomSearchPMTester();
		rs.launchRandomPMTest();
		
		/*for (int i = 0; i < operators.length; i++) {
			AbstractFMOperatorPerfMTest operator = operators[i];
			System.out.println(
					"OPERATOR: " + (i + 1) + "/" + operators.length + " NAME: " + operator.getClass().getSimpleName());
			launch(operator);
			operators[i] = null;
			operator = null;
		}*/

	}

	private static void launch(AbstractFMOperatorPerfMTest operator) {
		double[][] EXPERIMENTAL_DATA_PVALS = new double[NUMS_OF_EXPERIMENT_RUNS][AbstractFMOperatorPerfMTest.NUMBER_OF_FMs];
		double[][] EXPERIMENTAL_DATA_TTESTVALS = new double[NUMS_OF_EXPERIMENT_RUNS][AbstractFMOperatorPerfMTest.NUMBER_OF_FMs];
		long[][] EXPERIMENTAL_DATA_AVERAGE_FM = new long[NUMS_OF_EXPERIMENT_RUNS][AbstractFMOperatorPerfMTest.NUMBER_OF_FMs];
		long[][] EXPERIMENTAL_DATA_AVERAGE_FMFU = new long[NUMS_OF_EXPERIMENT_RUNS][AbstractFMOperatorPerfMTest.NUMBER_OF_FMs];
		boolean[][] EXPERIMENTAL_DATA_VERDICTS = new boolean[NUMS_OF_EXPERIMENT_RUNS][AbstractFMOperatorPerfMTest.NUMBER_OF_FMs];
		boolean[][] EXPERIMENTAL_DATA_ISNORMAL = new boolean[NUMS_OF_EXPERIMENT_RUNS][AbstractFMOperatorPerfMTest.NUMBER_OF_FMs];

		StringBuilder csv = new StringBuilder();
		csv.append("Operation");
		csv.append(";");
		csv.append("runNumber");
		csv.append(";");
		csv.append("tcNumber");
		csv.append(";");
		csv.append("pval");
		csv.append(";");
		csv.append("ttestval");
		csv.append(";");
		csv.append("avg. time fm");
		csv.append(";");
		csv.append("avg. time fmu");
		csv.append(";");
		csv.append("verdict");
		csv.append(";");
		csv.append("normality");
		csv.append("\n");

		for (int i = 0; i < NUMS_OF_EXPERIMENT_RUNS; i++) {
			System.out.println("RUN: " + (i + 1) + "/" + NUMS_OF_EXPERIMENT_RUNS);

			operator.test();

			for (int j = 0; j < AbstractFMOperatorPerfMTest.NUMBER_OF_FMs; j++) {
				EXPERIMENTAL_DATA_PVALS[i][j] = operator.EXPERIMENT_pvalVals[j];
				EXPERIMENTAL_DATA_TTESTVALS[i][j] = operator.EXPERIMENT_ttestVals[j];
				EXPERIMENTAL_DATA_AVERAGE_FM[i][j] = operator.EXPERIMENT_averageTimeVals_fmTimes[j];
				EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j] = operator.EXPERIMENT_averageTimeVals_fmfuTimes[j];
				EXPERIMENTAL_DATA_VERDICTS[i][j] = operator.EXPERIMENT_verdictsVals[j];
				EXPERIMENTAL_DATA_ISNORMAL[i][j] = operator.EXPERIMENT_isNormalVals[j];

				csv.append(operator.getClass().getSimpleName());
				csv.append(";");
				csv.append(i);
				csv.append(";");
				csv.append(j);
				csv.append(";");
				csv.append(df3.format(EXPERIMENTAL_DATA_PVALS[i][j]));
				csv.append(";");
				csv.append(df3.format(EXPERIMENTAL_DATA_TTESTVALS[i][j]));
				csv.append(";");
				csv.append(df3.format(EXPERIMENTAL_DATA_AVERAGE_FM[i][j]));
				csv.append(";");
				csv.append(df3.format(EXPERIMENTAL_DATA_AVERAGE_FMFU[i][j]));
				csv.append(";");
				csv.append((EXPERIMENTAL_DATA_VERDICTS[i][j]) ? "PASS" : "FAIL");
				csv.append(";");
				csv.append(EXPERIMENTAL_DATA_ISNORMAL[i][j]);
				csv.append("\n");
			}
		}
		double percFlaky = getPercentageOfFlakyTests(EXPERIMENTAL_DATA_VERDICTS);
		double percStatisticalSign = getPercentageOfStatisticalSignificance(EXPERIMENTAL_DATA_PVALS);

		csv.append("\npercFlaky;" + df3.format(percFlaky));
		csv.append("\npercStatisticalSign;" + df3.format(percStatisticalSign));

		File output = new File("output/EXPERIMENTS");// + this.getClass().getSimpleName());
		if (!output.exists()) {
			output.mkdirs();
		}

		File newFile = new File(output, operator.getClass().getSimpleName() + ".csv");
		Utils.writeStringToFile(newFile, csv.toString());
	}

	/**
	 * Percentage of statistical significance
	 * 
	 * @param pvals
	 * @return
	 */
	static double getPercentageOfStatisticalSignificance(double[][] pvals) {
		int numsOfStatisticalSignificanceTests = 0;
		for (int i = 0; i < pvals.length; i++) {
			for (int j = 0; j < pvals[0].length; j++) {
				if (pvals[i][j] < 0.05)
					numsOfStatisticalSignificanceTests++;

			}
		}
		return (double) numsOfStatisticalSignificanceTests / (double) (pvals[0].length * pvals.length);
	}

	/**
	 * Percentage of flaky tests
	 * 
	 * @param verdicts
	 * @return
	 */
	static double getPercentageOfFlakyTests(boolean[][] verdicts) {
		int numsofFlakyTests = 0;

		for (int testCase = 0; testCase < verdicts[0].length; testCase++) {

			int numsOfFails = 0;
			int numsOfPass = 0;
			for (int run = 0; run < verdicts.length; run++) {
				if (verdicts[run][testCase] == true) {
					numsOfPass++;
				} else {
					numsOfFails++;
				}
			}
			if (numsOfFails > 0 && numsOfPass > 0)
				numsofFlakyTests++;

		}
		return ((double) numsofFlakyTests / (double) verdicts[0].length);
	}

}
